FROM jenkins/jenkins:lts

USER root

# Install .NET Core
# TODO: Add mono, until FAKE 5 is released
RUN apt-get install -y wget && rm -rf /var/lib/apt/lists/*

RUN wget https://download.microsoft.com/download/1/1/5/115B762D-2B41-4AF3-9A63-92D9680B9409/dotnet-sdk-2.1.4-linux-x64.tar.gz \
    && mkdir -p /var/dotnet

RUN tar zxf dotnet-sdk-2.1.4-linux-x64.tar.gz -C /var/dotnet \
    && export PATH=$PATH:/var/dotnet

RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF \
    && echo "deb http://download.mono-project.com/repo/ubuntu trusty main" | sudo tee /etc/apt/sources.list.d/mono-official.list \
    && apt-get update \
    && apt-get install mono-devel \
    && rm -rf /var/lib/apt/lists/*

USER jenkins